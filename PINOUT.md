# Pin-out

## M5Stack LoRa868

| LoRa pin   | M5Stack bus | FireBeetle |
|------------|-------------|------------|
| Reset      | GPIO26      | GPIO26     |
| IRQ / DIO0 | GPIO36      | GPIO36     |
| CS / NSS   | GPIO33      | GPIO25     |
| MOSI       | GPIO23      | GPIO23     |
| MISO       | GPIO38      | GPIO19     |
| SCK        | GPIO18      | GPIO18     |
