// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "proto_files/boomlamp.pb.h"

#include <Arduino.h>
#include <LoRa.h>

#include <stdbool.h>
#include <stdint.h>

/**
 * Set up LoRa device, returning success (true) or failure (false).
 *
 * This just interfaces with the global LoRa instance. Use that after calling
 * this function.
 */
bool lora_setup(int8_t sck, int8_t miso, int8_t mosi, int8_t nssCS,
                int8_t reset, int8_t dio0IRQ, long frequency, bool isGateway);

void lora_update();

// Send a message via LoRa.
void lora_send(const BoomLampMessage &message);

// Returns whether the queue is empty and no transmission is happening at the
// moment.
bool lora_txQueueEmpty();

void lora_set_spreadingFactor(int sf);
int lora_get_spreadingFactor();

void lora_set_signalBandwidth(long bw);
long lora_get_signalBandwidth();
