// SPDX-License-Identifier: GPL-2.0-or-later
#include "time.h"

#include <Arduino.h>

namespace {
time_ms_t time_offset_;
}

void time_setup() {}

time_ms_t time_current() { return millis() + time_offset_; }

void time_set(time_ms_t time) { time_offset_ = time - millis(); }
