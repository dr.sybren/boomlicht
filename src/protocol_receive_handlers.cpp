// SPDX-License-Identifier: GPL-2.0-or-later

#include "protocol_receive_handlers.h"
#include "light.h"
#include "lora_wrap.h"
#include "m5core.h"
#include "protocol.h"
#include "time.h"

#include <Arduino.h>

bool on_diag_ping(const BoomLampMessage &message) {
  Serial.println("Received PING, returning PONG");
  proto_sendDiag_pong(message.diag.timestamp);
  return true;
}

bool on_diag_ohai(const BoomLampMessage &message) {
  Serial.printf("Received OHAI from %llx", message.srcMac);
#if defined(LORA_IS_GATEWAY)
  Serial.println(", going to sync time (I'm a gateway)");
  proto_sendDiag_setTime(time_current());
#else
  Serial.println(", sending a PING back (I'm not a gateway)");
  proto_sendDiag_ping(time_current());
#endif
  return true;
}

bool on_diag_time(const BoomLampMessage &message) {
  Serial.println("Received TIME, syncing up");
  const time_ms_t before = time_current();
  time_set(message.diag.timestamp);
  const time_ms_t after = time_current();

  Serial.print("Old local time: ");
  Serial.println(before);
  Serial.print("New local time: ");
  Serial.println(after);

  return true;
}

bool on_diag_pong(const BoomLampMessage &message) {
  Serial.print("Received PONG, ");
  const uint32_t now = time_current();

  Serial.print("checking time of flight: ");
  const uint32_t flightTime = now - message.diag.timestamp;
  Serial.print(flightTime);
  Serial.println(" ms");

#if defined(ARDUINO_M5STACK_Core2)
  m5core2_update_display_ping(flightTime);
#endif

  return true;
}

bool on_diag_radio(const BoomLampMessage &message) {
  if (!message.diag.has_radio) {
    return false;
  }

  Serial.println("RADIO switching:");
  Serial.printf("    SF: %lu -> %lu\r\n", lora_get_spreadingFactor(),
                message.diag.radio.spreadingFactor);
  Serial.printf("    BW: %lu -> %lu\r\n", lora_get_signalBandwidth(),
                message.diag.radio.signalBandwidth);

  lora_set_signalBandwidth(message.diag.radio.signalBandwidth);
  lora_set_spreadingFactor(message.diag.radio.spreadingFactor);

  // FIXME: For some reason, after switching bandwidth the reception seems to be
  // an issue, until we send out something.
  Serial.println("Sending a PING to test the new radio parameters");
  proto_sendDiag_ping(time_current());

  return true;
}

bool on_diag_reboot(const BoomLampMessage &message) {
  Serial.println("Device will reboot soon");
  delay(1000);
  esp_restart();
}

bool on_light_explicit(const BoomLampMessage &message) {
  return lightSetExplicit(message.light);
}
bool on_light_storeIndex(const BoomLampMessage &message) {
  return lightStoreIndex(message.light);
}
bool on_light_receiveIndex(const BoomLampMessage &message) {
  return lightRecallIndex(message.light);
}
