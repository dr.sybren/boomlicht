// SPDX-License-Identifier: GPL-2.0-or-later

#include "light.h"
#include "hsv.h"
#include "m5core.h"

#include <Arduino.h>
#include <map>

namespace {

// TODO: store this in non-volatile memory.
light::ColourMap colourMap_;

void logColour(const ExplicitColour &colour);
bool displayColour(const ExplicitColour &colour);

} // namespace

bool lightSetExplicit(const LightMessage &light) {
  if (!light.has_colour) {
    return false;
  }
  return displayColour(light.colour);
}

bool lightStoreIndex(const LightMessage &light) {
  if (!light.has_colour || !light.has_colourIndex)
    return false;

  Serial.println("Storing colour ");
  logColour(light.colour);
  Serial.printf(" as index %lu\r\n", light.colourIndex);

  colourMap_[light.colourIndex] = light.colour;
  return true;
}

bool lightRecallIndex(const LightMessage &light) {
  if (!light.has_colourIndex)
    return false;

  const auto iter = colourMap_.find(light.colourIndex);
  if (iter == colourMap_.end()) {
    Serial.printf("Recall index: no colour stored with index %lu\r\n",
                  light.colourIndex);
    return false;
  }

  return displayColour(iter->second);
}

namespace {

void logColour(const ExplicitColour &colour) {
  Serial.printf("[H=%.3f S=%.3f V=%.3f]", colour.h, colour.s, colour.v);
}

bool displayColour(const ExplicitColour &colour) {
  HSV hsv = {colour.h, colour.s, colour.v};
  RGB rgb;
  hsv_to_rgb(hsv, rgb);

#if defined(ARDUINO_M5STACK_Core2)
  m5core2_leds_main(rgb[0], rgb[1], rgb[2]);
  return true;
#else
  Serial.println("no support for explict lights on this board!");
  return false;
#endif
}

} // namespace
