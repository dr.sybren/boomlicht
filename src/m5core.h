// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#if defined(ARDUINO_M5STACK_Core2)

#include <stdbool.h>
#include <stdint.h>

#include "time.h"

void m5core2_setup();
void m5core2_update();

void m5core2_update_display_rssi(int rssi);
void m5core2_update_display_snr(float snr);
void m5core2_update_display_freqErr(long frequencyError);
void m5core2_update_display_ping(uint32_t flightTime);

void m5core2_leds_setup();
void m5core2_leds_error();
void m5core2_leds_blink_msg_received(bool handledOK);
void m5core2_leds_main(uint8_t r, uint8_t g, uint8_t b);

#endif
