// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once

#include <stdint.h>

using time_ms_t = uint32_t;

void time_setup();
time_ms_t time_current();
void time_set(time_ms_t time);
