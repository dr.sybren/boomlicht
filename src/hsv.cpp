// SPDX-License-Identifier: GPL-2.0-or-later

#include "hsv.h"

#include <math.h>

void hsv_to_rgb(HSV hsv, RGB r_rgb) {
  // Source: https://www.rapidtables.com/convert/color/hsv-to-rgb.html
  const float c = hsv[1] * hsv[2];
  const float h = hsv[0];
  const int h360 = h * 360;
  const float h_mod = fabs(fmod(h * 360.0f / 60.0f, 2) - 1);
  const float x = c * (1.0f - h_mod);
  const float m = hsv[2] - c;

  float r_, g_, b_;
  if (h360 < 60) {
    r_ = c;
    g_ = x;
    b_ = 0;
  } else if (h360 < 120) {
    r_ = x;
    g_ = c;
    b_ = 0;
  } else if (h360 < 180) {
    r_ = 0;
    g_ = c;
    b_ = x;
  } else if (h360 < 240) {
    r_ = 0;
    g_ = x;
    b_ = c;
  } else if (h360 < 300) {
    r_ = x;
    g_ = 0;
    b_ = c;
  } else {
    r_ = c;
    g_ = 0;
    b_ = x;
  }

  r_rgb[0] = 255 * (r_ + m);
  r_rgb[1] = 255 * (g_ + m);
  r_rgb[2] = 255 * (b_ + m);
}
