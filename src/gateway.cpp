// SPDX-License-Identifier: GPL-2.0-or-later

#include "gateway.h"
#include "light.h"
#include "lora_wrap.h"
#include "protocol.h"
#include "time.h"

#include <Arduino.h>

/**
 * The code in this file is intended to be replaced with external controls.
 * Could be a GUI, or another networked/serial device.
 */

namespace {

enum eMenuMode {
  MENU_COLOUR = 1,
  MENU_RADIO = 2,
};

eMenuMode menuMode = MENU_COLOUR;

// Command handlers, return true iff the command was handled.
bool menuUpdate_common(char command);
bool menuUpdate_colour(char command);
bool menuUpdate_radio(char command);

void show_help();
void send_ping();
void send_pong();
void send_ohai();
void sync_time();
void show_radio_config();

void send_reboot();
void reboot_now() __attribute__((noreturn));

// Colour parameters:
ExplicitColour currentColour;
light::ColourMap colourMap;
void send_colour(float h, float s, float v);
void send_colorStoreIndex(uint32_t index);
void send_colorRecallIndex(uint32_t index);
void send_colourMap();
void show_currentColour();
void show_colour(const ExplicitColour &colour);

constexpr float STEP_HUE = 0.025;
constexpr float STEP_SAT = 0.1;
constexpr float STEP_VALUE = 0.1;

// How often radio parameter changes are repeated, before switching over.
constexpr uint8_t RADIO_REPEATS = 5;

struct GatewayRadioParams {
  uint32_t spreadingFactor;
  uint32_t bandwidth;
};

void send_radio_spread(uint8_t spreadingFactor);
void send_radio_bandwidth(uint32_t bandwidth);
void send_radio_params(GatewayRadioParams *params);
void task_send_radio_params(void *pvParameters);

// !=1, @=2, etc. Out-of-bounds is returned as -1.
int8_t shiftnumToNum(char key);

} // namespace

void gateway_setup() {}

void gateway_showHelp() { show_help(); }

void gateway_update() {
  if (!Serial.available())
    return;

  const char command = Serial.read();
  if (menuUpdate_common(command))
    return;

  switch (menuMode) {
  case MENU_COLOUR:
    if (menuUpdate_colour(command))
      return;
    break;
  case MENU_RADIO:
    if (menuUpdate_radio(command))
      return;
    break;
  }

  Serial.println("Unknown input ");
  if (command < 32) {
    Serial.printf("[#%d]", command);
  } else {
    Serial.printf("[%c]", command);
  }
  Serial.println(", use [h] for help");
}

namespace {

bool menuUpdate_common(const char command) {
  switch (command) {
  case '?':
    show_help();
    return true;
  case '`':
    reboot_now();
    return true;
  case '~':
    send_reboot();
    return true;
  case 'p':
    send_ping();
    return true;
  case 'P':
    send_pong();
    return true;
  case 'o':
    send_ohai();
    return true;
  case 't':
    sync_time();
    return true;
  default:
    return false;
  }
}

bool menuUpdate_colour(const char command) {
  switch (command) {
  case 9: // tab
    menuMode = MENU_RADIO;
    show_help();
    return true;
  case 'm':
    send_colourMap();
    return true;
  case ' ':
    send_colour(currentColour.h, currentColour.s, currentColour.v);
    return true;
  case 'H':
    send_colour(currentColour.h + STEP_HUE, currentColour.s, currentColour.v);
    return true;
  case 'h':
    send_colour(currentColour.h - STEP_HUE, currentColour.s, currentColour.v);
    return true;
  case 'S':
    send_colour(currentColour.h, currentColour.s + STEP_SAT, currentColour.v);
    return true;
  case 's':
    send_colour(currentColour.h, currentColour.s - STEP_SAT, currentColour.v);
    return true;
  case 'V':
    send_colour(currentColour.h, currentColour.s, currentColour.v + STEP_VALUE);
    return true;
  case 'v':
    send_colour(currentColour.h, currentColour.s, currentColour.v - STEP_VALUE);
    return true;
  case 'a':
    send_colour(0.064f, 1.0f, 1.0f); // amber
    return true;
  case 'w':
    send_colour(0.000f, 0.0f, 1.0f); // white
    return true;
  case 'W':
    send_colour(0.000f, 0.0f, 0.0f); // black
    return true;
  case 'r':
    send_colour(0.000f, 1.0f, 1.0f); // red
    return true;
  case 'R':
    send_colour(0.500f, 1.0f, 1.0f); // cyan
    return true;
  case 'g':
    send_colour(0.333f, 1.0f, 1.0f); // green
    return true;
  case 'G':
    send_colour(0.757f, 0.775f, 0.933f); // magenta
    return true;
  case 'b':
    send_colour(0.666f, 1.0f, 1.0f); // blue
    return true;
  case 'B':
    send_colour(0.133f, 0.775f, 0.933f); // yellow
    return true;
  }

  if (command >= '0' && command <= '9') {
    const uint8_t index = (command - '0');
    send_colorRecallIndex(index);
    return true;
  }
  {
    const int8_t index = shiftnumToNum(command);
    if (index >= 0) {
      send_colorStoreIndex(index);
      return true;
    }
  }

  return false;
}

bool menuUpdate_radio(const char command) {
  switch (command) {
  case 9: // tab
    menuMode = MENU_COLOUR;
    show_help();
    return true;
  // Radio Spreading Factor:
  case '6':
    send_radio_spread(6);
    return true;
  case '7':
    send_radio_spread(7);
    return true;
  case '8':
    send_radio_spread(8);
    return true;
  case '9':
    send_radio_spread(9);
    return true;
  case '0':
    send_radio_spread(10);
    return true;
  case '1':
    send_radio_spread(11);
    return true;
  case '2':
    send_radio_spread(12);
    return true;
  // Radio Signal Bandwidth:
  case '!': // Shift+1
    send_radio_bandwidth(7800);
    return true;
  case '@': // Shift+2
    send_radio_bandwidth(10400);
    return true;
  case '#': // Shift+3
    send_radio_bandwidth(15600);
    return true;
  case '$': // Shift+4
    send_radio_bandwidth(20800);
    return true;
  case '%': // Shift+5
    send_radio_bandwidth(312500);
    return true;
  case '^': // Shift+6
    send_radio_bandwidth(41700);
    return true;
  case '&': // Shift+7
    send_radio_bandwidth(62500);
    return true;
  case '*': // Shift+8
    send_radio_bandwidth(125000);
    return true;
  case '(': // Shift+9
    send_radio_bandwidth(250000);
    return true;
  case ')': // Shift+0
    send_radio_bandwidth(500000);
    return true;
  case 'c':
    show_radio_config();
    return true;
  default:
    return false;
  }
}

void show_help() {
  Serial.println("==========================================");
  Serial.print("BOOMLICHT ");
#if defined(LORA_IS_GATEWAY)
  Serial.print("GATEWAY");
#else
  Serial.print("NODE");
#endif
  Serial.print(" - ");
  switch (menuMode) {
  case MENU_COLOUR:
    Serial.println("COLOUR");
    break;
  case MENU_RADIO:
    Serial.println("RADIO");
    break;
  }
  Serial.println("==========================================");
  Serial.println("?  : show this help");
  Serial.println("o  : send Diag  - ohai");
  Serial.println("p/P: send Diag  - ping / pong");
  Serial.println("t  : sync time");
  Serial.println("`/~: reboot this / remote device(s)");
  Serial.println("----------------------------");

  switch (menuMode) {
  case MENU_COLOUR:
    Serial.println("a  : send Light - amber");
    Serial.println("r/R: send Light - red / cyan");
    Serial.println("g/G: send Light - green / magenta");
    Serial.println("b/B: send Light - blue / yellow");
    Serial.println("w/W: send Light - white / black");
    Serial.println("h/H: decrease/increase hue");
    Serial.println("s/S: decrease/increase saturation");
    Serial.println("v/V: decrease/increase value");
    Serial.println("spc: re-send current colour");
    Serial.println("      0-9: recall indexed colour");
    Serial.println("shift+0-9: store indexed colour");
    Serial.println("        m: re-send colour map");
    Serial.println("tab: go to radio mode");
    break;
  case MENU_RADIO:
    Serial.println("6-0 & 1-2  : radio spreading factor 6-12");
    Serial.println("shift+[0-9]: radio bandwidth");
    Serial.println("c          : show local radio config");
    Serial.println("tab: go to colour mode");
    break;
  }

  Serial.println("==========================================");
}

void send_ping() {
  const uint32_t now = time_current();
  proto_sendDiag_ping(now);
}

void send_pong() {
  const uint32_t now = time_current();
  proto_sendDiag_pong(now);
}

void send_ohai() {
  const uint32_t now = time_current();
  proto_sendDiag_ohai(now);
}

void sync_time() {
  const uint32_t now = time_current();
  proto_sendDiag_setTime(now);
}

void send_reboot() {
  Serial.println("Requesting reboot of all listening devices");
  proto_sendDiag_reboot(time_current());
}

void reboot_now() {
  Serial.println("\r\nYou request death, we will rise again\r\n");
  delay(1000);
  esp_restart();
}

void send_colour(const float h, const float s, const float v) {
  currentColour.h = fmod(h + 1.0f, 1.0f);
  currentColour.s = fmax(0.0f, fmin(s, 1.0f));
  currentColour.v = fmax(0.0f, fmin(v, 1.0f));

  Serial.print("Sending ");
  show_currentColour();
  Serial.println();

  proto_sendLight_explicit(currentColour.h, currentColour.s, currentColour.v);
}

void send_colorStoreIndex(uint32_t index) {
  Serial.print("Storing ");
  show_currentColour();
  Serial.printf(" as index %lu\r\n", index);
  proto_sendLight_storeIndex(currentColour, index);

  colourMap[index] = currentColour;
}

void send_colorRecallIndex(uint32_t index) {
  Serial.printf("Recalling colour index %lu\r\n", index);
  proto_sendLight_recallIndex(index);

  if (colourMap.find(index) != colourMap.end()) {
    currentColour = colourMap[index];
  }
}

void send_colourMap() {
  Serial.println("Sending entire colour map");
  for (const auto entry : colourMap) {
    Serial.printf("  #%u = ", entry.first);
    show_colour(entry.second);
    Serial.println();
    proto_sendLight_storeIndex(entry.second, entry.first);
  }
  Serial.println("... done");
}

void show_currentColour() { show_colour(currentColour); }

void show_colour(const ExplicitColour &colour) {
  Serial.printf("[H=%.3f S=%.3f V=%.3f]", colour.h, colour.s, colour.v);
}

void send_radio_spread(const uint8_t spreadingFactor) {
  static GatewayRadioParams params;
  params.spreadingFactor = spreadingFactor;
  params.bandwidth = lora_get_signalBandwidth();
  send_radio_params(&params);
}

void send_radio_bandwidth(const uint32_t bandwidth) {
  static GatewayRadioParams params;
  params.spreadingFactor = lora_get_spreadingFactor();
  params.bandwidth = bandwidth;
  send_radio_params(&params);
}

void send_radio_params(GatewayRadioParams *params) {
  constexpr auto STACK_SIZE = 8192;
  const BaseType_t result =
      xTaskCreate(task_send_radio_params, "radio-params", STACK_SIZE, params,
                  tskIDLE_PRIORITY, nullptr);
  if (result == pdFAIL) {
    Serial.println("ERROR: unable to create background task");
  }
}

void task_send_radio_params(void *pvParameters) {
  const GatewayRadioParams *const params =
      static_cast<const GatewayRadioParams *>(pvParameters);

  Serial.printf("Sending radio spreading factor %lu and bandwidth %lu\r\n",
                params->spreadingFactor, params->bandwidth);

  // Repeat the new radio parameters a few times, to ensure nodes pick them
  // up.
  const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
  for (uint8_t i = 0; i < RADIO_REPEATS; ++i) {
    const time_ms_t now = time_current();
    proto_sendDiag_radio(now, params->spreadingFactor, params->bandwidth);
    vTaskDelay(xDelay);
  }

  // Wait for all the messages to go out, before switching to the new
  // parameters.
  Serial.println("Waiting for TX queue to clear...");
  const TickType_t queueDrainDelay = 100 / portTICK_PERIOD_MS;
  while (!lora_txQueueEmpty()) {
    vTaskDelay(queueDrainDelay);
  }

  Serial.println("Done sending new radio parameters, switching over myself");
  Serial.printf("    SF: %u -> %u\r\n", lora_get_spreadingFactor(),
                params->spreadingFactor);
  Serial.printf("    BW: %u -> %u\r\n", lora_get_signalBandwidth(),
                params->bandwidth);

  lora_set_signalBandwidth(params->bandwidth);
  lora_set_spreadingFactor(params->spreadingFactor);

  vTaskDelete(nullptr);
}

void show_radio_config() {
  Serial.println("Current local radio configuration:");
  Serial.printf("    SF: %u\r\n", lora_get_spreadingFactor());
  Serial.printf("    BW: %lu\r\n", lora_get_signalBandwidth());
}

int8_t shiftnumToNum(const char key) {
  switch (key) {
  case '!':
    return 1;
  case '@':
    return 2;
  case '#':
    return 3;
  case '$':
    return 4;
  case '%':
    return 5;
  case '^':
    return 6;
  case '&':
    return 7;
  case '*':
    return 8;
  case '(':
    return 9;
  case ')':
    return 0;
  default:
    return -1;
  }
}

} // namespace
