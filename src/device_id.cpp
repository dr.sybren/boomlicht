// SPDX-License-Identifier: GPL-2.0-or-later

#include "device_id.h"

#include <esp_mac.h>
#include <esp_wifi_types.h>

char DEVICE_MAC[18];
uint64_t DEVICE_ID;

namespace {
union macRepresentations {
  uint64_t device_id;
  uint8_t as_u8[6];
};
} // namespace

void device_id_setup() {
  macRepresentations mac = {0};
  esp_efuse_mac_get_default(reinterpret_cast<u_int8_t *>(&mac));

  DEVICE_ID = mac.device_id;
  sprintf(DEVICE_MAC, "%02X:%02X:%02X:%02X:%02X:%02X", mac.as_u8[0],
          mac.as_u8[1], mac.as_u8[2], mac.as_u8[3], mac.as_u8[4], mac.as_u8[5]);
}
