// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

void gateway_setup();
void gateway_update();

void gateway_showHelp();
