// SPDX-License-Identifier: GPL-2.0-or-later

#include "device_id.h"
#include "gateway.h"
#include "lora_wrap.h"
#include "protocol.h"

#include <Arduino.h>
#include <LoRa.h>

#if defined(ARDUINO_M5STACK_Core2)
#include "m5core.h"

#include <FastLED.h>
#include <M5Core2.h>
#endif

namespace {
const long frequency = 868E6;  // LoRa Frequency
const int csPin = LORA_PIN_CS; // LoRa radio chip select
const int sckPin = 18;
const int misoPin = LORA_PIN_MISO;
const int mosiPin = 23;
const int resetPin = 26; // LoRa radio reset
const int irqPin = 36;   // must be a hardware interrupt pin

volatile bool messageReceived = false;
volatile int receivedPacketSize = 0;

#ifdef USE_FASTLED
constexpr auto NUM_LEDS = 5;
CRGB leds[NUM_LEDS];
#endif

void setup_led();

void onReceive(int packetSize);
boolean runEvery(unsigned long interval);
void ledErrorMode();
void ledBlink(const bool handledOK);

// Wait a bit, then send an OHAI message. The delay helps to avoid a boot loop.
// Not exactly sure what causes it, but I think the LoRa IC needs some more time
// after a reset than we'd give it otherwise.
void send_boot_messages();
void task_send_boot_messages(void *pvParameters);

} // namespace

void setup() {
  Serial.begin(115200);
  while (!Serial)
    ;

  setup_led();
  device_id_setup();

#if defined(LORA_IS_GATEWAY)
  const bool lora_is_gateway = true;
#else
  const bool lora_is_gateway = false;
#endif
  const bool lora_ok = lora_setup(sckPin, misoPin, mosiPin, csPin, resetPin,
                                  irqPin, frequency, lora_is_gateway);

#if defined(ARDUINO_M5STACK_Core2)
  m5core2_setup();
#endif

  // LoRa.dumpRegisters(Serial);

  if (!lora_ok) {
    Serial.println("LoRa init failed. Check your connections.");
    Serial.println("halting in a busy-loop");
    ledErrorMode();
  }
  Serial.println("LoRa init succeeded.");
  Serial.println();

  // Show device mode & ID.
#if defined(LORA_IS_GATEWAY)
  gateway_setup();
  Serial.print("BoomLamp gateway starting, device MAC=");
#else
  Serial.print("BoomLamp node starting, device MAC=");
#endif
  Serial.print(DEVICE_MAC);
  Serial.printf("; ID=%llX\r\n", DEVICE_ID);
  Serial.println("Radio parameters:");
  Serial.printf("  SF=%lu\r\n", lora_get_spreadingFactor());
  Serial.printf("  BW=%lu Hz\r\n", lora_get_signalBandwidth());

  LoRa.onReceive(onReceive);

  send_boot_messages();
}

void loop() {
#if defined(ARDUINO_M5STACK_Core2)
  m5core2_update();
#endif

  if (messageReceived) {
    messageReceived = false;

    // Serial.println("Received packet:");
    const bool handledOK = proto_receiveMsg(receivedPacketSize);
    ledBlink(handledOK);

#if defined(ARDUINO_M5STACK_Core2)
    m5core2_update_display_rssi(LoRa.packetRssi());
    m5core2_update_display_snr(LoRa.packetSnr());
    m5core2_update_display_freqErr(LoRa.packetFrequencyError());
#endif
  }

  // #if defined(LORA_IS_GATEWAY)
  gateway_update();
  // #endif
  lora_update();
}

namespace {
void onReceive(const int packetSize) {
  receivedPacketSize = packetSize;
  messageReceived = true;
}

void setup_led() {
#if defined(ARDUINO_M5STACK_Core2)
  m5core2_leds_setup();
#else
  pinMode(BOARD_LED_PIN, OUTPUT);
#endif
}

void ledErrorMode() {
#if defined(ARDUINO_M5STACK_Core2)
  m5core2_leds_error();
  // if failed, do nothing
  while (true)
    ;
#else
  while (true) {
    digitalWrite(BOARD_LED_PIN, true);
    delay(500);
    digitalWrite(BOARD_LED_PIN, false);
    delay(500);
  }
#endif
}

void ledBlink(const bool handledOK) {
#if defined(ARDUINO_M5STACK_Core2)
  m5core2_leds_blink_msg_received(handledOK);
#else
  digitalWrite(BOARD_LED_PIN, true);
  delay(20);
  digitalWrite(BOARD_LED_PIN, false);
#endif
}

void send_boot_messages() {
  // Use a separate task at IDLE priority to do the delay & send the OHAI
  // message. This is necessary to avoid the boot loop; a straight delay(50)
  // call doesn't seem to cut it.
  constexpr auto STACK_SIZE = 8192;
  const BaseType_t result =
      xTaskCreate(task_send_boot_messages, "boot-messages", STACK_SIZE, nullptr,
                  tskIDLE_PRIORITY, nullptr);
  if (result == pdFAIL) {
    Serial.println(
        "ERROR: unable to create background task to send boot messages");
  }
}

void task_send_boot_messages(void *pvParameters) {
  const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
  vTaskDelay(xDelay);

  proto_sendDiag_ohai(time_current());

  vTaskDelay(xDelay);
  gateway_showHelp();

  vTaskDelete(nullptr);
}
} // namespace
