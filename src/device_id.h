// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <stdint.h>

extern char DEVICE_MAC[18];
extern uint64_t DEVICE_ID;

void device_id_setup();
