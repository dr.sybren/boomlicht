// SPDX-License-Identifier: GPL-2.0-or-later

#include "protocol.h"
#include "device_id.h"
#include "light.h"
#include "lora_wrap.h"
#include "proto_files/boomlamp.pb.h"
#include "protocol_receive_handlers.h"

#include <pb_decode.h>

#include <stdio.h>

uint32_t boomlamp_seq = 0;

namespace {

// Message handling functions, return true on successful handling, false
// otherwise.
bool handleReceivedMsg(const BoomLampMessage &message);
bool handleReceivedMsgLight(const BoomLampMessage &message);
bool handleReceivedMsgDiag(const BoomLampMessage &message);

BoomLampMessage newMsg(); // Create a new plain message without subtype.
BoomLampMessage newMsg_diag(DiagnosticMessage_Type type, uint32_t timestamp);
BoomLampMessage newMsg_light(LightMessage_Type type);

bool receive(int packetSizeInBytes, BoomLampMessage *r_message);

} // namespace

void proto_sendDiag_ping(const uint32_t timestamp) {
  BoomLampMessage message = newMsg_diag(DiagnosticMessage_Type_PING, timestamp);
  lora_send(message);
}

void proto_sendDiag_pong(const uint32_t timestamp) {
  BoomLampMessage message = newMsg_diag(DiagnosticMessage_Type_PONG, timestamp);
  lora_send(message);
}

void proto_sendDiag_setTime(const uint32_t timestamp) {
  BoomLampMessage message =
      newMsg_diag(DiagnosticMessage_Type_SET_TIME, timestamp);
  lora_send(message);
}

void proto_sendDiag_ohai(const uint32_t timestamp) {
  BoomLampMessage message = newMsg_diag(DiagnosticMessage_Type_OHAI, timestamp);
  lora_send(message);
}

void proto_sendDiag_reboot(const uint32_t timestamp) {
  BoomLampMessage message =
      newMsg_diag(DiagnosticMessage_Type_REBOOT, timestamp);
  lora_send(message);
}

void proto_sendDiag_radio(const uint32_t timestamp,
                          const uint32_t spreadingFactor,
                          const uint32_t bandwidth) {
  BoomLampMessage message =
      newMsg_diag(DiagnosticMessage_Type_RADIO, timestamp);
  message.diag.has_radio = true;
  message.diag.radio.spreadingFactor = spreadingFactor;
  message.diag.radio.signalBandwidth = bandwidth;
  lora_send(message);
}

void proto_sendLight_explicit(const float h, const float s, const float v) {
  BoomLampMessage message = newMsg_light(LightMessage_Type_EXPLICIT);
  message.light.has_colour = true;
  message.light.colour.h = h;
  message.light.colour.s = s;
  message.light.colour.v = v;
  lora_send(message);
}

void proto_sendLight_storeIndex(const ExplicitColour colour,
                                const uint32_t index) {
  BoomLampMessage message = newMsg_light(LightMessage_Type_STORE_INDEX);
  message.light.has_colour = true;
  message.light.has_colourIndex = true;
  message.light.colour = colour;
  message.light.colourIndex = index;
  lora_send(message);
}

void proto_sendLight_recallIndex(const uint32_t index) {
  BoomLampMessage message = newMsg_light(LightMessage_Type_RECALL_INDEX);
  message.light.has_colourIndex = true;
  message.light.colourIndex = index;
  lora_send(message);
}

bool proto_receiveMsg(const int packetSize) {
  BoomLampMessage message;

  if (!receive(packetSize, &message))
    return false;

  return handleReceivedMsg(message);
}

namespace {

bool receive(int packetSizeInBytes, BoomLampMessage *r_message) {
  *r_message = BoomLampMessage_init_default;

  // TODO: avoid this buffer, see if we can read from LoRa directly.
  uint8_t buffer[128];
  for (int i = 0; i < packetSizeInBytes; i++) {
    buffer[i] = LoRa.read();
  }

  pb_istream_t input = pb_istream_from_buffer(buffer, packetSizeInBytes);

  if (!pb_decode(&input, BoomLampMessage_fields, r_message)) {
    Serial.printf("Protobuf decode failed: %s\r\n", PB_GET_ERROR(&input));
    return false;
  }

  return true;
}

bool handleReceivedMsg(const BoomLampMessage &message) {
  Serial.printf("MSG from %llX: seq: %u; ", message.srcMac, message.seq);

  switch (message.type) {
  case BoomLampMessage_Type_ACK: // Unhandled for now.
    Serial.println("type: ACK");
    return false;
  case BoomLampMessage_Type_LIGHT:
    Serial.print("type: LIGHT; ");
    return handleReceivedMsgLight(message);
  case BoomLampMessage_Type_DIAG:
    Serial.print("type: DIAG; ");
    return handleReceivedMsgDiag(message);
  default:
    Serial.printf("type: UNKNOWN (%d)\r\n", message.type);
    return false;
  }
}

bool handleReceivedMsgDiag(const BoomLampMessage &message) {
  if (!message.has_diag) {
    Serial.print("NO DIAG PAYLOAD");
    return false;
  }

  const DiagnosticMessage &diag = message.diag;
  Serial.printf("timestamp=%u; ", diag.timestamp);

  switch (diag.type) {
  case DiagnosticMessage_Type_PING:
    Serial.println("PING");
    return on_diag_ping(message);
  case DiagnosticMessage_Type_PONG:
    Serial.println("PONG");
    return on_diag_pong(message);
  case DiagnosticMessage_Type_SET_TIME:
    Serial.println("SET_TIME");
    return on_diag_time(message);
  case DiagnosticMessage_Type_OHAI:
    Serial.println("OHAI");
    return on_diag_ohai(message);
  case DiagnosticMessage_Type_RADIO:
    Serial.println("RADIO");
    return on_diag_radio(message);
  case DiagnosticMessage_Type_REBOOT:
    Serial.println("REBOOT");
    return on_diag_reboot(message);
  default:
    Serial.printf("UNKNOWN (%d)\r\n", diag.type);
    return false;
  }
}

bool handleReceivedMsgLight(const BoomLampMessage &message) {
  if (!message.has_light) {
    Serial.print("NO LIGHT PAYLOAD");
    return false;
  }

  const LightMessage &light = message.light;
  switch (light.type) {
  case LightMessage_Type_EXPLICIT:
    Serial.println("EXPLICIT");
    return on_light_explicit(message);
  case LightMessage_Type_STORE_INDEX:
    Serial.println("STORE_INDEX");
    return on_light_storeIndex(message);
  case LightMessage_Type_RECALL_INDEX:
    Serial.println("RECALL_INDEX");
    return on_light_receiveIndex(message);
  default:
    Serial.printf("UNKNOWN TYPE (%d)\r\n", light.type);
    return false;
  }
}

BoomLampMessage newMsg() {
  BoomLampMessage message = BoomLampMessage_init_default;
  message.seq = ++boomlamp_seq;
  message.srcMac = DEVICE_ID;
  return message;
}

BoomLampMessage newMsg_diag(const DiagnosticMessage_Type type,
                            const uint32_t timestamp) {
  BoomLampMessage message = newMsg();
  message.type = BoomLampMessage_Type_DIAG;
  message.has_diag = true;
  message.diag.type = type;
  message.diag.timestamp = timestamp;
  return message;
}

BoomLampMessage newMsg_light(const LightMessage_Type type) {
  BoomLampMessage message = newMsg();
  message.type = BoomLampMessage_Type_LIGHT;
  message.has_light = true;
  message.light.type = type;
  return message;
}

} // namespace
