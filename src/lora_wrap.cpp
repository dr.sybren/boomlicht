// SPDX-License-Identifier: GPL-2.0-or-later

#include "lora_wrap.h"

#include <Arduino.h>
#include <LoRa.h>
#include <SPI.h>
#include <queue>

#include <pb_encode.h>

namespace {

bool isGateway_ = false;
volatile bool isTransmitting_ = false;
std::queue<BoomLampMessage> txQueue;

uint32_t nextAllowedTxTime = 0;
const uint32_t txThrottleTimeMs = 200;

// Radio parameters, at least as we understand them to be. The LoRa library
// doesn't allow reading.
int radioSpreadingFactor_ = 7;       // Power-on Default of SX1276 = 7
long radioSignalBandwidth_ = 500000; // Power-on Default of SX1276 = 125000

void onTxDone();

void lora_rxMode();
void lora_txMode();

bool maySend();

// Send without queueing.
void send_direct(const BoomLampMessage &message);

} // namespace

bool lora_setup(const int8_t sck, const int8_t miso, const int8_t mosi,
                const int8_t nssCS, const int8_t reset, const int8_t dio0IRQ,
                const long frequency, const bool isGateway) {

  isGateway_ = isGateway;

  SPI.begin(sck, miso, mosi, nssCS);
  LoRa.setSPI(SPI);
  LoRa.setPins(nssCS, reset, dio0IRQ);

  if (!LoRa.begin(frequency)) {
    return false;
  }

  LoRa.enableCrc();
  LoRa.setSignalBandwidth(radioSignalBandwidth_); // range 7800 - 500000.
  LoRa.setSpreadingFactor(radioSpreadingFactor_); // range [6, 12].

  LoRa.onTxDone(lora_rxMode);
  lora_rxMode();

  return true;
}

void lora_update() {
  if (!maySend() || txQueue.empty())
    return;

  Serial.printf("lora_update: queue has %d items, sending next message\r\n",
                txQueue.size());
  const BoomLampMessage next = txQueue.front();
  txQueue.pop();
  send_direct(next);
}

void lora_send(const BoomLampMessage &message) {
  if (maySend() && txQueue.empty()) {
    // Serial.println("lora_send: sending message directly");
    send_direct(message);
    return;
  }

  Serial.println("lora_send: queueing message");
  txQueue.push(message);
}

void lora_set_spreadingFactor(const int sf) {
  LoRa.setSpreadingFactor(sf);
  radioSpreadingFactor_ = sf;
}
void lora_set_signalBandwidth(const long bw) {
  LoRa.setSignalBandwidth(bw);
  radioSignalBandwidth_ = bw;
}
int lora_get_spreadingFactor() { return radioSpreadingFactor_; }
long lora_get_signalBandwidth() { return radioSignalBandwidth_; }

bool lora_txQueueEmpty() { return maySend() && txQueue.empty(); }

namespace {

void send_direct(const BoomLampMessage &message) {
  nextAllowedTxTime = millis() + txThrottleTimeMs;

  // TODO: avoid this buffer, see if we can encode to LoRa directly.
  uint8_t buffer[128];
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

  const bool encode_ok = pb_encode(&stream, BoomLampMessage_fields, &message);
  if (!encode_ok) {
    Serial.printf("Protobuf encode failed: %s\r\n", PB_GET_ERROR(&stream));
    return;
  }

  lora_txMode();
  LoRa.beginPacket();
  LoRa.write(buffer, stream.bytes_written);
  LoRa.endPacket(true);
}

void lora_rxMode() {
  if (isGateway_) {
    LoRa.disableInvertIQ();
  } else {
    LoRa.enableInvertIQ();
  }
  LoRa.receive();
  isTransmitting_ = false;
}

void lora_txMode() {
  isTransmitting_ = true;
  LoRa.idle();
  if (isGateway_) {
    LoRa.enableInvertIQ();
  } else {
    LoRa.disableInvertIQ();
  }
}

bool maySend() { return !isTransmitting_ && nextAllowedTxTime < millis(); }

} // namespace
