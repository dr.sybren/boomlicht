// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once

#include <inttypes.h>

typedef float HSV[3];   // [0-1] range.
typedef uint8_t RGB[3]; // [0-255] range.

void hsv_to_rgb(HSV hsv, RGB r_rgb);
