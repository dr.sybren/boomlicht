// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "proto_files/boomlamp.pb.h"

#include <stdbool.h>
#include <stdint.h>

bool on_diag_ping(const BoomLampMessage &message);
bool on_diag_pong(const BoomLampMessage &message);
bool on_diag_time(const BoomLampMessage &message);
bool on_diag_ohai(const BoomLampMessage &message);
bool on_diag_radio(const BoomLampMessage &message);

// Reboot the ESP32.
bool on_diag_reboot(const BoomLampMessage &message) __attribute__((noreturn));

bool on_light_explicit(const BoomLampMessage &message);
bool on_light_storeIndex(const BoomLampMessage &message);
bool on_light_receiveIndex(const BoomLampMessage &message);
