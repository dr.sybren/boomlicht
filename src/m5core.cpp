// SPDX-License-Identifier: GPL-2.0-or-later

#if defined(ARDUINO_M5STACK_Core2)
#include "m5core.h"
#include "device_id.h"
#include "lora_wrap.h"
#include "protocol.h"
#include "time.h"

#include <FastLED.h>
#include <M5Core2.h>
#include <SPI.h>
#include <lvgl.h>

#undef lcd

namespace {

constexpr uint16_t colorBG = TFT_PURPLE;
constexpr uint16_t fontScale = 2;
constexpr uint16_t fontHeight = 8 * fontScale;

TFT_eSPI tft = TFT_eSPI();
lv_disp_buf_t disp_buf;
lv_color_t buf[LV_HOR_RES_MAX * 10];

lv_obj_t *rssi_label = nullptr;
lv_obj_t *snr_label = nullptr;
lv_obj_t *freqErr_label = nullptr;
lv_obj_t *rssi_bar = nullptr;
lv_obj_t *cb_dim_screen = nullptr;
lv_obj_t *cb_ping = nullptr;
lv_obj_t *power_label = nullptr;
lv_obj_t *ping_label = nullptr;

volatile bool is_pinging_ = false;

// FastLED
constexpr auto NUM_LEDS = 10;
CRGB leds[NUM_LEDS];

void setup_power() {
  M5.Axp.SetLcdVoltage(3000);

  M5.Axp.SetBusPowerMode(0);
  M5.Axp.SetCHGCurrent(AXP192::kCHG_190mA);

  // Vibrate the vibrator.
  // M5.Axp.SetLDOEnable(3, true);
  // delay(150);
  // M5.Axp.SetLDOEnable(3, false);
}

bool my_touchpad_read(lv_indev_drv_t *indev_driver, lv_indev_data_t *data) {
  TouchPoint_t pos = M5.Touch.getPressPoint();
  const bool touched = (pos.x == -1) ? false : true;
  if (!touched) {
    data->state = LV_INDEV_STATE_REL;
  } else {
    data->state = LV_INDEV_STATE_PR;
    /*Set the coordinates*/
    data->point.x = pos.x;
    data->point.y = pos.y;
  }
  return false;
  // Return `false` because we are not buffering and no more data to read
}

void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area,
                   lv_color_t *color_p) {
  const uint32_t w = (area->x2 - area->x1 + 1);
  const uint32_t h = (area->y2 - area->y1 + 1);
  tft.startWrite();
  tft.setAddrWindow(area->x1, area->y1, w, h);
  tft.pushColors(&color_p->full, w * h, true);
  tft.endWrite();
  lv_disp_flush_ready(disp);
}

void setScreenDimming(const bool dim) {
  M5.Axp.SetLcdVoltage(dim ? 2500 : 3300);
}

void on_dim_screen_evt_checkbox(lv_obj_t *obj, lv_event_t event) {
  if (event != LV_EVENT_VALUE_CHANGED)
    return;

  const bool isChecked = lv_checkbox_is_checked(obj);
  setScreenDimming(isChecked);
}

void on_dim_screen_evt_button(Event &event) {
  switch (event.type) {
  // case E_TAP:
  // case E_PRESSED:
  case E_TOUCH:
    // LVGL doesn't call the above callback for programmatic changes.
    const bool setChecked = !lv_checkbox_is_checked(cb_dim_screen);
    lv_checkbox_set_checked(cb_dim_screen, setChecked);
    setScreenDimming(setChecked);
  }
}

void setIsPinging(const bool ping) { is_pinging_ = ping; }

void on_ping_evt_checkbox(lv_obj_t *obj, lv_event_t event) {
  if (event != LV_EVENT_VALUE_CHANGED)
    return;

  const bool isChecked = lv_checkbox_is_checked(obj);
  setIsPinging(isChecked);
}

void on_ping_evt_button(Event &event) {
  switch (event.type) {
  // case E_TAP:
  // case E_PRESSED:
  case E_TOUCH:
    // LVGL doesn't call the above callback for programmatic changes.
    const bool setChecked = !lv_checkbox_is_checked(cb_ping);
    lv_checkbox_set_checked(cb_ping, setChecked);
    setIsPinging(setChecked);
  }
}

void setup_lvgl() {
  lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);
  lv_init();

  //-------------------------------------------------------------------
  /*Initialize the display*/
  lv_disp_drv_t disp_drv;
  lv_disp_drv_init(&disp_drv);
  disp_drv.hor_res = 320;
  disp_drv.ver_res = 240;
  disp_drv.flush_cb = my_disp_flush;
  disp_drv.buffer = &disp_buf;
  lv_disp_drv_register(&disp_drv);

  //-------------------------------------------------------------------
  /*Initialize the (dummy) input device driver*/
  lv_indev_drv_t indev_drv;
  lv_indev_drv_init(&indev_drv);
  indev_drv.type = LV_INDEV_TYPE_POINTER;
  indev_drv.read_cb = my_touchpad_read;
  lv_indev_drv_register(&indev_drv);
}

void setup_gui() {
  /*Halo on press*/
  static lv_style_t style_halo;
  lv_style_init(&style_halo);
  lv_style_set_transition_time(&style_halo, LV_STATE_PRESSED, 400);
  lv_style_set_transition_time(&style_halo, LV_STATE_DEFAULT, 0);
  lv_style_set_transition_delay(&style_halo, LV_STATE_DEFAULT, 200);
  lv_style_set_outline_width(&style_halo, LV_STATE_DEFAULT, 0);
  lv_style_set_outline_width(&style_halo, LV_STATE_PRESSED, 20);
  lv_style_set_outline_opa(&style_halo, LV_STATE_DEFAULT, LV_OPA_COVER);
  /*Just to be sure, the theme might use it*/
  lv_style_set_outline_opa(&style_halo, LV_STATE_FOCUSED, LV_OPA_COVER);
  lv_style_set_outline_opa(&style_halo, LV_STATE_PRESSED, LV_OPA_TRANSP);
  lv_style_set_transition_prop_1(&style_halo, LV_STATE_DEFAULT,
                                 LV_STYLE_OUTLINE_OPA);
  lv_style_set_transition_prop_2(&style_halo, LV_STATE_DEFAULT,
                                 LV_STYLE_OUTLINE_WIDTH);

  static lv_style_t header_style;
  lv_style_init(&header_style);
  lv_style_set_text_font(&header_style, LV_STATE_DEFAULT,
                         &lv_font_montserrat_20);
  lv_style_set_text_color(&header_style, LV_STATE_DEFAULT, LV_COLOR_MAGENTA);

  constexpr auto margin_top = 10;
  constexpr auto margin_left = 10;
  constexpr auto font_size = 20;
  constexpr auto padding_v = 4;

  lv_obj_t *label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text_static(label, "LoRa Test");
  lv_label_set_align(label, LV_LABEL_ALIGN_LEFT);
  lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left, margin_top);
  lv_obj_add_style(label, LV_LABEL_PART_MAIN, &header_style);

  rssi_label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(rssi_label, "RSSI: ??");
  lv_obj_align(rssi_label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + 2 * font_size + padding_v);

  static lv_style_t rssi_bar_style_bg;
  lv_style_init(&rssi_bar_style_bg);
  lv_style_set_bg_color(&rssi_bar_style_bg, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  static lv_style_t rssi_bar_style_indic;
  lv_style_init(&rssi_bar_style_indic);
  lv_style_set_bg_color(&rssi_bar_style_indic, LV_STATE_DEFAULT,
                        LV_COLOR_PURPLE);

  rssi_bar = lv_bar_create(lv_scr_act(), NULL);
  lv_bar_set_range(rssi_bar, -120, 0);
  lv_bar_set_value(rssi_bar, -120, LV_ANIM_OFF);
  lv_bar_set_anim_time(rssi_bar, 250);
  lv_obj_set_width(rssi_bar, LV_HOR_RES_MAX / 2);
  lv_obj_align(rssi_bar, NULL, LV_ALIGN_IN_TOP_RIGHT, -margin_left,
               margin_top + 2 * font_size + padding_v);
  lv_obj_add_style(rssi_bar, LV_BAR_PART_BG, &rssi_bar_style_bg);
  lv_obj_add_style(rssi_bar, LV_BAR_PART_INDIC, &rssi_bar_style_indic);

  cb_dim_screen = lv_checkbox_create(lv_scr_act(), NULL);
  lv_checkbox_set_text_static(cb_dim_screen, "Dim Screen");
  lv_obj_align(cb_dim_screen, NULL, LV_ALIGN_IN_BOTTOM_LEFT, margin_left,
               -margin_top);
  lv_obj_set_event_cb(cb_dim_screen, on_dim_screen_evt_checkbox);
  M5.BtnA.addHandler(on_dim_screen_evt_button);

  cb_ping = lv_checkbox_create(lv_scr_act(), NULL);
  lv_checkbox_set_text_static(cb_ping, "Ping");
  lv_obj_align(cb_ping, NULL, LV_ALIGN_IN_BOTTOM_RIGHT, -margin_left,
               -margin_top);
  lv_obj_set_event_cb(cb_ping, on_ping_evt_checkbox);
  M5.BtnC.addHandler(on_ping_evt_button);

  snr_label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(snr_label, "SNR : ??");
  lv_obj_align(snr_label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + 3 * font_size + padding_v);

  freqErr_label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(freqErr_label, "FreqErr: ??");
  lv_obj_align(freqErr_label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + 4 * font_size + padding_v);

  // label = lv_label_create(lv_scr_act(), NULL);
  // lv_label_set_text_fmt(label, "MAC: %s", DEVICE_MAC);
  // lv_label_set_align(label, LV_LABEL_ALIGN_LEFT);
  // lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
  //              margin_top + 5 * font_size + padding_v);

  ping_label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(ping_label, "Ping: ??? ms");
  lv_obj_align(ping_label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + 5 * font_size + padding_v);

  power_label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(power_label, "Battery: ???");
  lv_obj_align(power_label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + 7 * font_size + padding_v);
}

// void onConnectionNew(const WifiMesh::NodeID nodeID) {
//   Serial.printf("new connection from node %u\r\n", nodeID);
//   WifiMesh::MeshNode &node = WifiMesh::nodeInfo(nodeID);
//   node.guiElement = lv_list_add_btn(list_nodes, NULL, node.name.c_str());
// }

// void onConnectionLost(const WifiMesh::NodeID nodeID) {
//   WifiMesh::MeshNode &node = WifiMesh::nodeInfo(nodeID);
//   Serial.printf("lost connection to node %u / %s\r\n", nodeID,
//                 node.name.c_str());

//   const auto index = lv_list_get_btn_index(list_nodes, node.guiElement);
//   lv_list_remove(list_nodes, index);
//   node.guiElement = nullptr;
// }
// void onNodeInfo(const WifiMesh::NodeID nodeID) {
//   WifiMesh::MeshNode &node = WifiMesh::nodeInfo(nodeID);
//   lv_obj_t *label = lv_list_get_btn_label(node.guiElement);
//   lv_label_set_text(label, node.name.c_str());
// }

void update_power_info(lv_task_t *task) {
  const float batLevel = M5.Axp.GetBatteryLevel();
  const float batUseCurrent = M5.Axp.GetBatCurrent();
  const float batChargeCurrent = M5.Axp.GetBatChargeCurrent();
  const float vInCurrent = M5.Axp.GetVinCurrent();
  const bool isCharging = batChargeCurrent > 0.01f;

  lv_label_set_text_fmt(power_label, //
                        "Battery: %d%% (%s %d mA)\nUSB Current: %d mA",
                        int(batLevel),                     //
                        isCharging ? "charging" : "using", //
                        isCharging ? int(batChargeCurrent)
                                   : int(batUseCurrent), //
                        int(vInCurrent));
}

void task_keepPinging(void *) {
  constexpr TickType_t delayTicks = 2000 / portTICK_PERIOD_MS;

  for (;;) {
    vTaskDelay(delayTicks);
    if (!is_pinging_)
      continue;

    if (!lora_txQueueEmpty())
      continue;

    proto_sendDiag_ping(time_current());
  }

  vTaskDelete(nullptr);
}

void setup_tasks() {
  lv_task_create(update_power_info, 2000, LV_TASK_PRIO_LOWEST, nullptr);

  constexpr auto STACK_SIZE = 4096;
  const BaseType_t result =
      xTaskCreate(task_keepPinging, "radio-params", STACK_SIZE, nullptr,
                  tskIDLE_PRIORITY, nullptr);
  if (result == pdFAIL) {
    Serial.println(
        "ERROR: unable to create background task for periodic pinging");
  }
}

} // namespace

void m5core2_setup() {
  M5.begin(true,  // LCDEnable
           false, // SDEnable
           false, // SerialEnable false; it's already set up
           false  // I2CEnable, disable as it uses GPIO33 as well.
  );
  setup_power();
  setup_lvgl();
  setup_gui();
  setup_tasks();
}

void m5core2_update() {
  M5.update();
  lv_task_handler();
}

void m5core2_update_display_rssi(const int rssi) {
  lv_label_set_text_fmt(rssi_label, "RSSI: %d", rssi);
  lv_bar_set_value(rssi_bar, rssi, LV_ANIM_ON);
}

void m5core2_update_display_snr(const float snr) {
  const int snrInt = snr;
  const int snrFrac = 100 * (snr - snrInt);
  lv_label_set_text_fmt(snr_label, "SNR : %d.%02d", snrInt, snrFrac);
}

void m5core2_update_display_freqErr(const long frequencyError) {
  lv_label_set_text_fmt(freqErr_label, "FreqE: %ld", frequencyError);
}

void m5core2_update_display_ping(const uint32_t flightTime) {
  lv_label_set_text_fmt(ping_label, "Ping: %lu ms", flightTime);
}

void m5core2_leds_setup() {
  FastLED.addLeds<SK6822, G25, GRB>(leds, NUM_LEDS);
  FastLED.clear();
  FastLED.show();
}

void m5core2_leds_error() {
  for (int i = 0; i < NUM_LEDS; i++)
    leds[i] = CRGB::DarkRed;
  FastLED.show();
}

void m5core2_leds_blink_msg_received(bool handledOK) {
  // First & last LED = status
  leds[0] = leds[NUM_LEDS - 1] = handledOK ? CRGB::Green : CRGB::Red;
  FastLED.show();

  FastLED.delay(20);

  leds[0] = leds[NUM_LEDS - 1] = CRGB::Black;
  FastLED.show();
}

void m5core2_leds_main(uint8_t r, uint8_t g, uint8_t b) {
  // LED 1..N-2 = main lights.
  for (int i = 1; i < NUM_LEDS - 1; i++) {
    leds[i].setRGB(r, g, b);
  }
  FastLED.show();
}

#endif
