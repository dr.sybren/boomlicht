// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "proto_files/boomlamp.pb.h"

#include <map>
#include <stdbool.h>

// Handle a light message. Return true if handled OK, false otherwise.
bool lightSetExplicit(const LightMessage &light);
bool lightStoreIndex(const LightMessage &light);
bool lightRecallIndex(const LightMessage &light);

// Mapping from colour index to actual colour.
namespace light {
using ColourMap = std::map<uint32_t, ExplicitColour>;
}
