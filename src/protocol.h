// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "proto_files/boomlamp.pb.h"

// All 'bool' functions return success (true) or failure (false).

// Diagnostic functions.
void proto_sendDiag_ping(uint32_t timestamp);
void proto_sendDiag_pong(uint32_t timestamp);
void proto_sendDiag_setTime(uint32_t timestamp);
void proto_sendDiag_ohai(uint32_t timestamp);
void proto_sendDiag_reboot(uint32_t timestamp);

void proto_sendDiag_radio(uint32_t timestamp, uint32_t spreadingFactor,
                          uint32_t bandwidth);

// Light functions.
void proto_sendLight_explicit(float h, float s, float v);
void proto_sendLight_storeIndex(ExplicitColour colour, uint32_t index);
void proto_sendLight_recallIndex(uint32_t index);

// Handle received LoRa protobuf message.
bool proto_receiveMsg(int packetSize);
